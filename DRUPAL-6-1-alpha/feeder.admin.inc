<?php

/**
*@file
* Include file for feeder.
* @author aradhana
* @date 19/01/2009
*/

/*
 * Callback for feeder seetings
 */
function feeder_admin_settings()
{
    $feed_default_limit = variable_get('feed_default_limit', '');
	$feed_default_icon_image = variable_get('feed_image_default_path', '');
    $feed_google_analytics_trackercode = variable_get('feed_google_analytics_trackercode', '');
    $feed_google_analytics_domain = variable_get('feed_google_analytics_domain', '');
    $feed_caching_hours = variable_get("feed_caching_hours", '6');
    //$feed_default_icon_image = db_fetch_array(db_query("Select * from variable where name = 'feed_default_rss_icon'"));
    $form['feed_default_limit'] = array('#type' => 'textfield',
    '#title' => t('Default Limit for feeds'),
    '#default_value' => "{$feed_default_limit}",
    '#description' => t('Default Limit for feeds.'),
    );
    $form['feed_image_default_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Default image path for RSS Icon'),
    '#default_value' => $feed_default_icon_image,
    );
    $form['google_analytic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Details of Google Analytics account for tracking purpose'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('If you want to make your rss link to be tracked by Google Analytics; provide your code and domain that is being used by google analytics.<br /> This option is optional '),
    );
    $form['google_analytic']['feed_google_analytics_trackercode'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Analytics tracker code'),
    '#default_value' => $feed_google_analytics_trackercode,
    );
    $form['google_analytic']['feed_google_analytics_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Analytics domain name'),
    '#default_value' => $feed_google_analytics_domain,
    );
    $form['feed_caching_hours'] = array(
    '#type' => 'textfield',
    '#title' => t('Caching hours'),
    '#default_value' => $feed_caching_hours,
    '#description' => t('Value in hours for which RSS feeds needs to be cached. Default value is 6.'),
    );

  return system_settings_form($form);
}

function feeder_feed_list() {
    $result = db_query('SELECT * FROM {feeder} ORDER BY fdid');
    $rows = array();
    while ($feeder_feed = db_fetch_object($result)) {
        $rows[] = array($feeder_feed->feed_title, $feeder_feed->feed_path, l(t('edit'), 'admin/build/feeder/edit/'. $feeder_feed->fdid), l(t('delete'), 'admin/build/feeder/delete/'. $feeder_feed->fdid));
    }
    $header = array(t('Feed Title'), t('Feed Path'), array('data' => t('Operations'), 'colspan' => 2));

    return theme('table', $header, $rows);
}

function feeder_feed_op_validate($form, &$form_state)
{
	if ($form_state['values']['feeder_op'] == 'edit') {
	  $exists_feed_name = db_fetch_object(db_query("Select * from {feeder} where feed_name = '%s' and fdid<>'%d'", $form_state['values']['feed_name'], $form_state['values']['fdid']));

	}
	else{
      $exists_feed_name = db_fetch_object(db_query("Select * from {feeder} where feed_name = '%s'", $form_state['values']['feed_name']));

      $exists_path_name = db_fetch_object(db_query("Select * from {menu_router} where path = '%s'", $form_state['values']['feed_path']));
      $exists_path_router_name = db_fetch_object(db_query("Select * from {menu_links} where router_path = '%s'", $form_state['values']['feed_path']));
	}

	if ( !empty($exists_feed_name) ) {
      form_set_error('feed_name', t('Feed Name should be unique. This feed name already exists.'));
    }
    if ( !empty($exists_feed_name) || !empty($exists_path_router_name) ) {
      form_set_error('feed_path', t('Feed Path already exists.'));
    }
}
function feeder_feed_op_submit(&$form, &$form_state)
{
    if ($form_state['values']['feeder_op'] == 'add') {
        if( empty($form_state['values']['limit']) ) {
            $feeder_feed_limit = variable_get('feed_default_limit', 10);
            $form_state['values']['limit'] = $feeder_feed_limit;
        }
        $feed_generation_time = time();
        $query = db_query("INSERT INTO {feeder}(feed_name,feed_path,feed_title,feed_limit,feed_query,channel_link,channel_title,channel_copyright,feed_generation_time,feed_duration_time) values('%s','%s','%s','%d','%s','%s','%s','%s','%s','%s')",$form_state['values']['feed_name'],$form_state['values']['feed_path'],$form_state['values']['feed_title'],$form_state['values']['limit'],$form_state['values']['feed_query'],$form_state['values']['link'],$form_state['values']['title'],$form_state['values']['copyright'],$feed_generation_time,$form_state['values']['feed_duration_time']);
        if ($query) {
            drupal_set_message(t('Feed %title has been added.', array('%title' => $form_state['values']['feed_title'])));
            menu_rebuild();
        }
        //watchdog('mail', 'Contact form: category %category added.', array('%category' => $form_state['values']['category']), WATCHDOG_NOTICE, l(t('view'), 'admin/build/contact'));

    }
    else {
        $query = db_query(" Update {feeder} set feed_name = \"%s\",
                                     feed_path = \"%s\",
                                     feed_title = \"%s\",
                                     feed_limit = \"%d\",
                                     feed_query = \"%s\",
                                     channel_link = \"%s\",
                                     channel_title = \"%s\",
                                     feed_duration_time = \"%s\",
                                     channel_copyright = \"%s\" where fdid = {$form_state['values']['fdid']}",$form_state['values']['feed_name'],$form_state['values']['feed_path'],$form_state['values']['feed_title'],$form_state['values']['limit'],$form_state['values']['feed_query'],$form_state['values']['link'],$form_state['values']['title'],$form_state['values']['feed_duration_time'],$form_state['values']['copyright']);
        if ($query) {
            drupal_set_message(t('Feed %title has been updated.', array('%title' => $form_state['values']['feed_title'])));
            menu_rebuild();
        }

        // watchdog('mail', 'Contact form: category %category updated.', array('%category' => $form_state['values']['category']), WATCHDOG_NOTICE, l(t('view'), 'admin/build/contact'));
    }

    $form_state['redirect'] = 'admin/build/feeder';
    return;
}

function feeder_admin_delete(&$form_state, $fdid) {
    $feeder_feed = db_fetch_array(db_query("Select * from feeder where fdid = '%d'",$fdid));

    $form['feeder'] = array(
    '#type' => 'value',
    '#value' => $feeder_feed,
    );

    return confirm_form($form, t("Are you sure you want to delete?"), 'admin/build/feeder', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process category delete form submission.
 */
function feeder_admin_delete_submit($form, &$form_state) {
    $feeder_feed = $form_state['values']['feeder'];
    db_query("DELETE FROM {feeder} WHERE fdid = %d", $feeder_feed['fdid']);
    drupal_set_message(t('Feed %feed_title has been deleted.', array('%feed_title' => $feeder_feed['feed_title'])));
   // watchdog('mail', 'Contact form: category %category deleted.', array('%category' => $contact['category']), WATCHDOG_NOTICE);

    $form_state['redirect'] = 'admin/build/feeder';
    return;
}

function feeder_feed_op($form_state = array(), $op='', $fdid = NULL){

    $feeder_feed = array();
    $feeder_feed['feed_name'] = '';
    $feeder_feed['feed_title'] = '';
    $feeder_feed['feed_path'] = '';
    $feeder_feed['feed_limit'] = '';
    $feeder_feed['feed_query'] = '';
    $feeder_feed['feed_duration_time'] = '';
    $feeder_feed['channel_link'] = '';
    $feeder_feed['channel_title'] = '';
    $feeder_feed['channel_copyright'] = '';
    if ( !empty($fdid) ) {
        $feeder_feed = db_fetch_array(db_query("Select * from {feeder} where fdid = '%d'",$fdid));
    }

    $form['feeder_op'] = array('#type' => 'value', '#value' => $op);
    $form['fdid'] = array('#type' => 'value', '#value' => $fdid);

    $form['feed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Feed Settings'),
    '#prefix' => '',
    );

    $form['feed']['feed_name'] = array('#type' => 'textfield',
    '#title' => t('Feed Name'),
    '#maxlength' => 255,
    '#default_value' => $feeder_feed['feed_name'],
    '#description' => t(" Unique feed name to be used. e.g 'earth_matters' "),
    '#required' => TRUE,
    );
    $form['feed']['feed_title'] = array('#type' => 'textfield',
    '#title' => t('Feed Title'),
    '#maxlength' => 255,
    '#default_value' => $feeder_feed['feed_title'],
    '#description' => t(" Feed title to be displayed alongwith the feed icon. e.g 'Earth Matters'"),
    '#required' => TRUE,
    );
    $form['feed']['feed_path'] = array('#type' => 'textfield',
    '#title' => t('Feed Url'),
    '#maxlength' => 255,
    '#default_value' => $feeder_feed['feed_path'],
    '#description' => t('Feed path to be used.'),
    '#required' => TRUE,
    );
    $form['feed']['feed_query'] = array('#type' => 'textarea',
    '#title' => t('Feed Query'),
    '#default_value' => $feeder_feed['feed_query'],
    '#description' => t('query to be used.'),
    '#required' => TRUE,
    );
    $form['feed']['limit'] = array('#type' => 'textfield',
    '#title' => t('Number of items to be shown'),
    '#default_value' => $feeder_feed['feed_limit'],
    '#description' => t('If you want to use default limit, leave this field empty.'),
    );
    $form['feed']['feed_duration_time'] = array('#type' => 'select',
    '#title' => t('Feed duration time'),
    '#options' => array('past_7_days' => t('Past 7 Days'), 'day' => t('Current Day'), 'week' => t('Current Week'), 'month' => t('Current Month'), 'year' => t('Current Year')),
    '#default_value' => $feeder_feed['feed_duration_time'],
     '#required' => True,
    );

    $form['channel'] = array(
    '#type' => 'fieldset',
    '#title' => t('Feed Channel Settings'),
    '#prefix' => '',
    );
    $form['channel']['link'] = array('#type' => 'textfield',
    '#title' => t('Channel Link'),
    '#default_value' => $feeder_feed['channel_link'],
     '#required' => True,
    );
    $form['channel']['title'] = array('#type' => 'textfield',
    '#title' => t('Channel Title'),
    '#default_value' => $feeder_feed['channel_title'],
    '#required' => True,
    );
    $form['channel']['copyright'] = array('#type' => 'textfield',
    '#title' => t('Channel Copyright'),
    '#default_value' => $feeder_feed['channel_copyright'],
    '#required' => True,
    );
    $form['submit'] = array('#type' => 'submit',
    '#value' => t('Save'),
    );
    return $form;
}